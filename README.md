# Troubleshooting Cheat Sheet
## Events
### Get Cluster Events
For the entire cluster:

```
oc get events -A --sort-by=".lastTimestamp"
```

For a specific namespace:

```
oc get events -n <namespace> --sort-by=".lastTimestamp"
```

### Watch Cluster Events
For the entire cluster:

```
oc get events -A --watch-only
```

For a specific namespace:

```
oc get events -n <namespace> --watch-only
```

## Nodes
### Get Node Information
```
oc describe node
```

For usage info:

```
oc adm top nodes
```

### Debug Node
```
oc debug node/<node_name>
chroot /host
```

### Debug kubelet and cri-o
```
systemctl status [kubelet, crio]
```

```
oc adm node-logs <node_name> -u [kubelet, crio]
```

### Get kernel logs
```
oc debug node/<node_name>
chroot /host
journalctl -k -e
```

## Pods
### Get pod details
```
oc describe pod <pod_name> -n <namespace>
```

### List containers in a pod
```
oc get pod <pod_name> -n <namespace> -o jsonpath='{.spec.containers[*].name}{"\n"}'
```

### Get pod logs
Get all logs:

```
oc logs pod/<pod_name> -c <container_name> -n <namespace>
```

Follow logs:

```
oc logs pod/<pod_name> -c <container_name> -n <namespace> -f --since=1m
```

Get logs from previous container instance:

```
oc logs pod/<pod_name> -c <container_name> -n <namespace> -p
```

### Copy file to pod
```
oc cp <local_path> <pod_name>:/<path> -c <container_name> -n <namespace>
```

### Copy file from a pod
```
oc cp <pod_name>:/<path> -c <container_name> <local_path> -n <namespace>
```

### Execute commands inside a pod
Run a single command:

```
oc exec <pod_name> -c <container_name> -n <namespace> -- cat /var/log/<path_to_log>
```

Get a bash shell:

```
oc exec <pod_name> -c <container_name> -n <namespace> -i -t -- bash -il
```

### Delete pods
Delete specific pod:

```
oc delete pod <pod_name> -n <namespace>
```

Delete all pods in a namespace:

```
oc delete --all pods -n <namespace>
```

## Gather support data
### Debug cluster operator
```
oc adm inspect clusteroperator/<operator_name>
```

### Collect must-gather information
```
oc adm must-gather
```
With Performance add-on information:
```
oc adm must-gather --image-stream=openshift/must-gather --image=registry.redhat.io/openshift4/performance-addon-operator-must-gather-rhel8:v4.9 
```
With SR-IOV information:
```
oc adm must-gather --image-stream=openshift/must-gather --image=quay.io/openshift/origin-sriov-operator-must-gather:4.9
```

### Collect user namespace information
```
oc adm inspect ns/<user_namespace>
```

### sosreport
```
oc debug node/<node_name>
chroot /host
toolbox
sosreport -k crio.all=on -k crio.logs=on 
```

## Get Cluster Config
### Kubelet config
```
oc describe kubeletconfig
```

### Performance profile
```
oc describe performanceprofile
```

### SR-IOV state
```
oc describe SriovNetworkNodeState -n openshift-sriov-network-operator
```

### Machine Config
List Machine Config Pools:
```
oc get mcp
```

Get Machine Config Pool details:
```
oc describe mcp <pool_name>
```

Get Machine Config details:
```
oc describe mc <machine_config>
```

### Installed Operators
List subscriptions:
```
oc get subs -A
```

Get subscription details:
```
oc describe sub <subscription_name> -n <namespace>
```

List installed operators:
```
oc get csv -l \!olm.copiedFrom -A
```

List available channels for a subscription:
```
oc get packagemanifests <operator_name> -ojson | jq '.status.channels[] | .currentCSV + " " + .name'
```

List operators with updates available:
```
oc get subs -A -o json | jq '.items[] | select (.status.state!="AtLatestKnown") | {name:.metadata.name, installed:.status.installedCSV,available:.status.currentCSV}'
```

### Storage
List storage classes:
```
oc get storageclass
```

List CSI drivers:
```
oc get csidriver
```

List PVCs:
```
oc get pvc -A
```

### Network
Get cluster network config:
```
oc describe network
```

### Certificates
List CSRs:
```
oc get csr
```

Approve pending CSRs:
```
oc get csr -o go-template='{{range .items}}{{if not .status}}{{.metadata.name}}{{"\n"}}{{end}}{{end}}' | xargs --no-run-if-empty oc adm certificate approve
```
